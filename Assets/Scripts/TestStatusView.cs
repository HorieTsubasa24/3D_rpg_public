﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestStatusView : MonoBehaviour {
    public CommonData cd;
    public GameObject prefab_HpText;
    public GameObject prefab_MpText;

    Text text_Hp;
    Text text_Mp;

    // Use this for initialization
    void Start ()
    {
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();

        /*
        text_Hp = Instantiate(prefab_HpText, GameObject.Find("Panel").transform).GetComponent<Text>();
        text_Mp = Instantiate(prefab_MpText, GameObject.Find("Panel").transform).GetComponent<Text>();
        */

        StartCoroutine(SetHpMpText());
    }
	
    IEnumerator SetHpMpText()
    {
        bool isTextSeted = false;
        while (isTextSeted == false)
        {
            if (cd.isEndRead == false)
            {
                yield return 0;
                continue;
            }

            //text_Hp.text = cd.PlayerData[0].HP.ToString() + " / " + cd.PlayerData[0].M_HP.ToString();
            //text_Mp.text = cd.PlayerData[0].MP.ToString() + " / " + cd.PlayerData[0].M_MP.ToString();

            isTextSeted = true;
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
