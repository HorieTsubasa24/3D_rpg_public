﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utage;
using System;

// Jsonにしようかなあ...
// 直列化

/// <summary>
/// 座標、体力、フラグなどの共通データは全てここに入れる。 
/// </summary>
[Serializable]
public class CommonData : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :           外部のクラス             :
    :                                    :
    /~----------------------------------*/
    // テーブル
    public PlayerTable players;
    public ItemTable items;
    public SkillTable skills;
    
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み完了
    /// </summary>
    public bool isEndRead = false;

    /// <summary>
    /// 読み込んだデータから主人公パラメータを生成
    /// </summary>
    private void Start()
    {
        StartCoroutine(InitPram());
    }

    /// <summary>
    /// cdObjectが作られて初めてロードする時
    /// </summary>
    /// <returns></returns>
    private IEnumerator InitPram()
    {
        // ここで全てのテーブル類の初期化を待つ
        while (!players || !items || !skills ||
                !players.isEndRead || !items.isEndRead || !skills.isEndRead)
        {
            yield return 0;
        }
        // EditorApplication.isPaused = true;

        for (int i = 0; i < pd.Length; i++)
            pd[i] = new PlayerData();

        PlayerDataSet();
        DontDestroyOnLoad(this);
        isEndRead = true;

        yield return 0;
    }

    /// <summary>
    /// lvは0から始まる
    /// </summary>
    /// <param name="lv"></param>
    public void PlayerDataSet(int lv = 0)
    {
        if (lv != 0) lv--;

        // 主人公のパラメーターセット
        for(int i = 0; i < pd.Length; i++)
        {
            // TODO 直す
            var dat = players.st_Aresta;
            pd[i].isJoined = true;

            // TODO テスト用なので消す
            // これは薬草と傷薬
            PlayerData[0].Ply_item[0] = items.items[0];
            PlayerData[0].Ply_item[1] = items.items[1];
            PlayerData[0].Ply_item[2] = items.items[1];

            // TODO テスト用なので消す
            // これはフレイム,フリーズ,キュア,フレイズ,ドラゴンサンダー
            PlayerData[0].Ply_skl[0] = skills.ply_skills[0];
            PlayerData[0].Ply_skl[1] = skills.ply_skills[1];
            PlayerData[0].Ply_skl[2] = skills.ply_skills[2];
            PlayerData[0].Ply_skl[3] = skills.ply_skills[3];
            PlayerData[0].Ply_skl[4] = skills.enm_skills[4];
            /*
            PlayerData[0].Ply_skl[5] = skills.ply_skills[5];
            PlayerData[0].Ply_skl[6] = skills.ply_skills[6];
            PlayerData[0].Ply_skl[7] = skills.ply_skills[7];
            PlayerData[0].Ply_skl[8] = skills.ply_skills[8];
            PlayerData[0].Ply_skl[9] = skills.enm_skills[9];*/

            if (i == 1)
            {
                dat = players.st_Erina;
                pd[i].isJoined = false;
            }
            if (i == 2)
            {
                dat = players.st_Petil;
                pd[i].isJoined = false;
            }
            pd[i].PlayerName = dat[lv].Name;
            pd[i].LV = dat[lv].LV;
            pd[i].HP = dat[lv].HP;
            pd[i].M_HP = dat[lv].HP;
            pd[i].MP = dat[lv].MP;
            pd[i].M_MP = dat[lv].MP;
            pd[i].STR = dat[lv].STR;
            pd[i].AGI = dat[lv].AGI;
            pd[i].INT = dat[lv].INT;
            pd[i].VIT = dat[lv].VIT;
            pd[i].LUK = dat[lv].LUK;
            pd[i].CRI = dat[lv].CRI;
            pd[i].EXP = dat[lv].EXP_ALL - dat[lv].EXP;
            pd[i].EXP_BTM = dat[lv].EXP_ALL - dat[lv].EXP;
            pd[i].EXP_UPR = dat[lv].EXP_ALL;
            pd[i].G = dat[lv].EXP;
        }
    }

    public void LvUp(int playerId)
    {

    }
    
    /*~----------------------------------/
    :                                    :
    :    マップに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 位置クラス
    /// </summary>
    [System.Serializable]
    public class Position
    {
        public int direction;
        public int x;
        public int y;

        public int floor;

        /// <summary>
        /// (x, y, 方向{0 ~ 3})
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="d"></param>
        public Position(int x, int y, int d, int fl)
        {
            this.x = x;
            this.y = y;
            this.direction = d;
            this.floor = fl;
        }

        /// <summary>
        /// フロア情報はそのままで、位置のみセット
        /// </summary>
        public void LocationSet(int x, int y, int d)
        {
            this.x = x;
            this.y = y;
            this.direction = d;
        }

    }
    
    /// <summary>
    /// 現在地の位置情報を丸めて情報にする
    /// </summary>
    public Vector3 TransformToNowPositionSet(GameObject ob)
    {
        // x, y, zの整数化
        var xyz = ob.transform.position;
        var x = (int)Math.Round(xyz.x);
        var y = (int)Math.Round(xyz.y);
        var z = (int)Math.Round(xyz.z);
        ob.transform.position = new Vector3(x, y, z);

        // rx, ry, rzの整数化
        var rot = ob.transform.localEulerAngles;
        var rx = (int)Math.Round(rot.x);
        var ry = (int)Math.Round(rot.y);
        var rz = (int)Math.Round(rot.z);
        ob.transform.rotation = Quaternion.Euler(rx, ry, rz);

        if (ry < 0) ry = -ry + 180;
        if (ry > 359) ry = 0;

        int d = ry / 90;

        return new Vector3(x, -z, d);
    }

    /// <summary>
    /// 外部から座標を参照するときに使用
    /// </summary>
    public Position intPos;
    public int Posx { get { return intPos.x; } }
    public int Posy { get { return intPos.y; } }
    public int Posd { get { return intPos.direction; } }

    /// <summary>
    /// 1フレーム前の座標
    /// </summary>
    public Position BeforeFlamePos;

    /// <summary>
    /// 前回の座標
    /// 外部から座標を参照するときに使用
    /// </summary>
    public Position BeforePosition;

    /// <summary>
    /// 現座標と前回座標の比較
    /// </summary>
    public bool CheckintPosEqBeforePos { get {return intPos.x == BeforePosition.x && intPos.y == BeforePosition.y; } }


    /// <summary>
    /// 他シーンからの復帰用マップ位置座標を一時的に記憶するプロパティ
    /// </summary>
    public Position RetPosition = new Position(-1, -1, 0, 1);


    /*~----------------------------------/
    :                                    :
    :    バトルに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 敵出現時、どの敵キャラグループを出現させるかを記憶してバトルシーンで配置
    /// </summary>
    public string BattleEnemyGroupName;
    
    const int NumPlayer = 3;


    /*~----------------------------------/
    :                                    :
    :    キャラクターに関係するデータ    :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// プレイヤーデータ(0 ~ 2)
    /// </summary>
    public PlayerData[] pd = new PlayerData[NumPlayer];

    /// <summary>
    /// 外部アクセス用
    /// </summary>
    public PlayerData[] PlayerData
    {
        get { return pd; }
        set { pd = value; }
    }

    /// <summary>
    /// 仲間の数
    /// </summary>
    public int PlayerNum {
        get {
            int a = 0;
            foreach (var p in pd)
                a += Convert.ToInt32(p.isJoined == true);

            return a;
        }
    }

    /// <summary>
    /// アレスタのデータ
    /// </summary>
    public PlayerData Aresta { get { return pd[0]; } set { pd[0] = value; } }
    /// <summary>
    /// エリナのデータ
    /// </summary>
    public PlayerData Erina { get { return pd[1]; } set { pd[1] = value; } }
    /// <summary>
    /// ペテルのデータ
    /// </summary>
    public PlayerData Petil { get { return pd[2]; } set { pd[2] = value; } }

    /*~----------------------------------/
    :                                    :
    :    フラグに関係するデータ          :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 1F
    /// (6, 9)
    /// アレスタ「いっくよー！」
    /// の終了判定
    /// </summary>
    public bool isStartMesAfter = false;


}
