﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Bracket = BracketCalculation;

public class ItemTable : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み終了
    /// </summary>
    public bool isEndRead;

    /// <summary>
    /// アイテムのテーブル
    /// </summary>
    public List<Item> items = new List<Item>();

    /// <summary>
    /// csv読み込み
    /// </summary>
    private void Awake()
    {
        FileStream table = new FileStream(Application.dataPath + "/StreamingAssets/Tables/ItemTable.csv", FileMode.Open, FileAccess.Read);
        try
        {
            //抜けると同時に削除
            using (StreamReader sr = new StreamReader(table))
            {
                //ファイルが空の場合
                if (table.Length == 0)
                {
                    print("ファイルが空でした。");
                    return;
                }

                //一行目はスルー
                sr.ReadLine();
                sr.ReadLine();


                //一行ごとに読み込める
                while (!sr.EndOfStream)
                {
                    // ファイルから一行読み込む
                    var line = sr.ReadLine();

                    // データを分ける
                    var ar = line.Split(',');

                    // 出力
                    foreach (var value in ar)
                    {
                        print(value + ", ");
                    }

                    Item.act mtd = Item.GetItemMethod(int.Parse(ar[0]));
                    items.Add(new Item(ar[0], ar[1], ar[2], ar[3], ar[4], ar[5], ar[6], ar[7], ar[8], ar[9], ar[10], mtd));

                }

            }
        }
        catch
        {
            print("アイテムデータ");
            print("読み込めませんでした。");
        }

        isEndRead = true;
    }

    /*~----------------------------------/
    :                                    :
    :    外部からスキルを取得する        :
    :                                    :
    /~----------------------------------*/
    public enum ItemID
    {
        none = 0, やくそう, 傷薬
    }

    public Item GetItemFromName(ItemID id)
    {
        int iid = (int)id;
        for (int i = 0; i < items.Count; i++)
        {
            if (iid == items[i].ItemID)
                return items[i];
        }

        return items[0];
    }
}

/*~----------------------------------/
:                                    :
:    アイテムそのものの構造体        :
:                                    :
/~----------------------------------*/
[System.Serializable]
public struct Item
{
    /// <summary>
    /// スキルの関数、威力計算に使う
    /// </summary>
    public delegate int act(Battle_Status b_st, Battle_Status b_st_tgt);

    /// <summary>
    /// アイテムの関数、威力計算に使う
    /// </summary>
    public act itm_method;

    /// <summary>
    /// アイテムの種類
    /// </summary>
    public enum Likes
    {
        Anytime, BattleOnly, MapOnly
    }

    /// <summary>
    /// アイテムの効果
    /// </summary>
    public enum Effect
    {
        Recover, StatusChange, Attack, Event
    }
    

    /// <summary>
    /// ターゲット
    /// </summary>
    public enum Tgt
    {
        Player, Players, Enemy, Enemys, Myself
    }

    /// <summary>
    /// アイテムの名前
    /// </summary>
    public string ItemName;

    /// <summary>
    /// アイテムのID、効果の呼び出しに使う
    /// </summary>
    public int ItemID;

    public Likes like;
    public Effect effect;
    public StatusTable.Attribute attribute;

    /// <summary>
    /// アイテムの効果量(effectの値で用途が変わる)
    /// </summary>
    public int effectAmount;

    /// <summary>
    /// ダメージ計算式
    /// </summary>
    public string DamageFormula;

    /// <summary>
    /// アイテム仕様時のメッセージ
    /// *Usr, *Tgtで使用者、対象者の名前を入れるように使う。
    /// 改行はCR(\r)で
    /// </summary>
    public string[] ItemMessages;

    /// <summary>
    /// アイテムの説明文
    /// </summary>
    public string ExplanatoryText;

    /// <summary>
    /// ターゲット
    /// </summary>
    public Tgt Target;

    /// <summary>
    /// エフェクトの名前
    /// </summary>
    public string EffectName;

    /// <summary>
    /// アイテム割当
    /// </summary>
    /// <param name="id"></param>
    /// <param name="Name"></param>
    /// <param name="likes"></param>
    /// <param name="amount"></param>
    /// <param name="Atr"></param>
    /// <param name="tgt"></param>
    /// <param name="Text"></param>
    public Item(string id, string Name, string amount, string likes, string efc, string atr, 
        string tgt, string Text, string efcname, string damage_formula, string exp_txt, act method)
    {
        int ID = int.Parse(id);
        int Amount = int.Parse(amount);
        int Likes = int.Parse(likes);
        int Effect = int.Parse(efc);
        int Tgt = int.Parse(tgt);
        int Attribute = int.Parse(atr);

        ItemID = ID;
        ItemName = Name;
        itm_method = method;
        Target = (Tgt)Tgt;
        like = (Likes)Likes;
        effect = (Effect)Effect;
        effectAmount = (int)Amount;
        DamageFormula = damage_formula;
        ExplanatoryText = exp_txt;
        attribute = (StatusTable.Attribute)Attribute;
        EffectName = efcname;

        var ary = Text.Split('\r');
        ItemMessages = new string[ary.Length];

        for (int i = 0; i < ary.Length; i++)
        {
            ItemMessages[i] = ary[i];
        }

    }

    /// <summary>
    /// アイテムのスキルの計算式を取得
    /// </summary>
    /// <returns></returns>
    public static act GetItemMethod(int id)
    {
        act[] ac = { null, YakusouLv1, YakusouLv2, YakusouLv3 };
        return ac[id];
    }

    /************************************
     * 
     *       アイテムスキル
     * 
     ***********************************/
    public static int YakusouLv1(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(Bracket.GetFormulaVal(b_st, b_st_tgt));
    }

    public static int YakusouLv2(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(Bracket.GetFormulaVal(b_st, b_st_tgt));
    }

    public static int YakusouLv3(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        return (int)(Bracket.GetFormulaVal(b_st, b_st_tgt));
    }

}
