﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class BracketCalculation {
    /*~----------------------------------/
    :                                    :
    :             固有式の計算           :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// ()括弧
    /// </summary>
    public class Brackets
    {
        public float? value = null;
        public int? Location = null;
        public int? EndLocation = null;
        public int? Level = null;
        public string str = "";
        public Brackets Parentheses = null;
        public List<Brackets> brc = new List<Brackets>();

        public Brackets(int loc, int lv)
        {
            Location = loc; Level = lv;
        }
    }


    /// <summary>
    /// 固有ダメージ計算式計算、値取得
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static float GetFormulaVal(Battle_Status b_st, Battle_Status b_st_tgt)
    {
        List<Brackets> brc = new List<Brackets>();
        string[] Reserved = { "(", ")",
                              " ", "+", "-", "*", "/", "enm_", "ply_",
                              "str", "agi", "int", "vit", "luk", "cri", "exp", "amo", "lv",
                              "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "." };

        string str;
        if (b_st.NextAct == Battle_Status.Act.Skill) str = "( " + b_st.SelectedSkl.DamageFormula + " )";
        else /*if (b_st.NextAct == Battle_Status.Act.Item)*/ str = "( " + b_st.SelectedItem.DamageFormula + " )";
        //else throw new FormatException("Item and Skill is not alive");

        int i = 0;
        int j = 0;

        int wdk = 0;
        // 括弧ごとに文字列を分ける
        while (str.Length > i)
        {
            if (str[i] == '(')
            {
                // i = number, k = level
                brc.Add(new Brackets(i, j));
                // 親の括弧に子の括弧を加える。
                for (int l = 0; l < brc.Count; l++)
                {
                    if (j == 0) break;
                    if (brc[l].Level == j - 1 && brc[l].EndLocation == null)
                    {
                        brc[l].brc.Add(brc[brc.Count - 1]);
                        break;
                    }
                }
                j++;
            }
            if (str[i] == ')')
            {
                for (int k = 0; k < brc.Count; k++)
                {
                    if (brc[k].Level == j - 1 && brc[k].EndLocation == null)
                    {
                        // 括弧内の文字取得
                        brc[k].EndLocation = i;
                        brc[k].str = str.ToString().Substring((int)brc[k].Location + 1, (int)brc[k].EndLocation - 1 - (int)brc[k].Location);
                        brc[k].Parentheses = GetParentheses(brc[k], brc);
                        break;
                    }
                }
                j--;
            }
            i++;
            wdk++;
            if (wdk > 5000)
                return -10000f;
        }
        brc = SortByLevel(brc);

        // 後ろのレベルの深い子括弧から計算 (親括弧に子括弧の情報があるので一つの値として計算し、
        // 最後に親括弧のvalueに値が来るようになる。)
        // キーワード取得
        i = brc.Count - 1;
        wdk = 0;
        while (i > -1)
        {
            List<string> keyword = new List<string>();

            while (0 < brc[i].str.Length)
            {
                keyword.Add(GetReservedWord(brc[i].str, Reserved));
                brc[i].str = brc[i].str.Remove(0, keyword[keyword.Count - 1].Length);
            }
            // 括弧単位の計算
            brc[i].value = CalculationInParentheses(keyword, b_st, brc[i]);
            i--;
            wdk++;
            if (wdk > 5000) return -2f;
        }
        return (float)brc[0].value;
    }

    static List<Brackets> SortByLevel(List<Brackets> brc)
    {
        int i = 0;
        int j = 1;
        int wdk = 0;
        Brackets a;
        while (i < brc.Count - j)
        {
            if (brc[i].Level > brc[i + 1].Level)
            {
                a = brc[i + 1];
                brc[i + 1] = brc[i];
                brc[i] = a;
            }
            i++;
            if (i == brc.Count - j)
            {
                i = 0;
                j++;
            }
            wdk++;
            if (wdk > 5000) return null;
        }
        return brc;
    }



    /// <summary>
    /// 親の括弧取得
    /// </summary>
    /// <param name="brc"></param>
    /// <param name="brcs"></param>
    /// <returns></returns>
    static Brackets GetParentheses(Brackets brc, List<Brackets> brcs)
    {
        if (brc.Level == 0) return null;
        for (int i = 0; i < brcs.Count; i++)
        {
            for (int j = 0; j < brcs[i].brc.Count; j++)
            {
                if (brcs[i].brc[j] == brc) return brcs[i];
            }
        }
        return null;
        throw new FormatException("GetParentheses was Faild");
    }

    /// <summary>
    /// 予約語取得
    /// </summary>
    /// <param name="str"></param>
    /// <param name="words"></param>
    /// <returns></returns>
    static string GetReservedWord(string str, string[] words)
    {
        for (int i = 0; i < words.Length; i++)
        {
            if (str.IndexOf(words[i]) == 0) return words[i];
        }
        throw new FormatException("GetReservedWord was Faild");
        // return -1;
    }

    /// <summary>
    /// 括弧単位の計算
    /// </summary>
    /// <param name="words"></param>
    /// <param name="bs"></param>
    /// <param name="brcs"></param>
    /// <returns></returns>
    static float CalculationInParentheses(List<string> words, Battle_Status bs, Brackets brc)
    {
        float? value = null;
        float? tgt_value = null;
        int brc_idx = 0;
        int opr = 0;
        int wdk = 0;

        // 括弧の中のワードをすべて見ていく
        while (words.Count > 0)
        {
            if (words[0].Length == 1)
            {
                int o = GetOperator(words[0].ToCharArray()[0]);
                // スペース
                if (o == 0)
                    words.RemoveAt(0);
                // 括弧はじめのときは子括弧の値取得
                else if (o == 5)
                {
                    o = 0;
                    tgt_value = (float)brc.brc[brc_idx].value;

                    // 括弧と括弧内全て削除
                    int brccnt = 0;
                    while (true)
                    {
                        if (words[0] == "(")
                        {
                            brccnt++;
                            words.RemoveAt(0);
                        }
                        else if (words[0] == ")")
                        {
                            words.RemoveAt(0);
                            if (brccnt == 1)
                                break;
                            else
                                brccnt--;
                        }
                        else
                        {
                            words.RemoveAt(0);
                        }
                        wdk++;
                        if (wdk > 10000) return -3f;
                    }
                    brc_idx++;
                }
                // 演算子取得
                else if (o != 0 && o < 0x10)
                {
                    opr = o;
                    words.RemoveAt(0);
                }
                // スペースか括弧終わりが出るまで数字取得
                else if (o >= 0x10)
                {
                    string n = "";
                    while (words.Count > 0 && words[0] != " " && words[0] != ")")
                    {
                        n += words[0];
                        words.RemoveAt(0);
                    }
                    //n += "f";
                    tgt_value = float.Parse(n);
                }
            }
            else
            {
                // 予約語からステータスの値を取得 必ず ply_?? enm_?? の形なので2word消去する。
                tgt_value = GetParameter(words[0], words[1], bs);
                words.RemoveAt(0);
                words.RemoveAt(0);

            }

            // 計算
            if (value != null && opr != 0 && tgt_value != null)
            {
                tgt_value = FourArithmeticOperations(opr, (float)value, (float)tgt_value);
                opr = 0;
            }
            if (tgt_value != null) value = tgt_value;
            tgt_value = null;

            wdk++;
            if (wdk > 10000) return -4f;
        }
        return (float)value;
    }


    static int GetOperator(Char ch)
    {
        char[] nums = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };
        switch (ch)
        {
            case ' ':
                return 0;
            case '+':
                return 1;
            case '-':
                return 2;
            case '*':
                return 3;
            case '/':
                return 4;
            case '(':
                return 5;
            case ')':
                return 6;
            default:
                for (int i = 0; i < nums.Length; i++)
                {
                    if (ch == nums[i]) return 0x10 + i;
                }
                return -10000;
                throw new FormatException("GetOperator was faild");
                // return -1;
        }
    }

    static float GetParameter(string st1, string st2, Battle_Status bs)
    {
        switch (st1)
        {
            case "enm_":
                return GetParameterInStatus(true, st2, bs);
            case "ply_":
                return GetParameterInStatus(false, st2, bs);
            default:
                return -10000;
                throw new FormatException("GetParameter was faild");
                // return -1;
        }
    }

    static float GetParameterInStatus(bool isEnm, string st, Battle_Status bs)
    {
        Battle_Status bs_tgt;
        bs_tgt = (isEnm == true) ? bs.EnmStatus[bs.SelectedTgtNum] : bs;

        switch (st)
        {
            case "str":
                return bs_tgt.B_Str;
            case "agi":
                return bs_tgt.B_Agi;
            case "int":
                return bs_tgt.B_Int;
            case "vit":
                return bs_tgt.B_Vit;
            case "luk":
                return bs_tgt.B_Luk;
            case "cri":
                return bs_tgt.B_Cri;
            case "exp":
                return bs_tgt.Exp;
            case "amo":
                return (bs.NextAct == Battle_Status.Act.Skill) ? bs.SelectedSkl.effectAmount : bs.SelectedItem.effectAmount;
            case "lv":
                return bs.Lv;
            default:
                throw new FormatException("GetParameterInStatus was faild");
                // return -1;
        }
    }

    public static float FourArithmeticOperations(int m, float a, float b)
    {
        switch (m)
        {
            case 1:
                return a + b;
            case 2:
                return a - b;
            case 3:
                return a * b;
            case 4:
                return a / b;
            default:
                return -10000;
                throw new FormatException("FourArithmeticOperations was faild");
                // return -1;
        }
    }
}
