﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Battle_Status : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :           メニューバー             :
    :                                    :
    /~----------------------------------*/
    public GameObject prefab_Menu;
    private GameObject ob_Menu;
    public Battle_MenuButton Menu;
    public ItemBook itemBook;
    public SkillBook skillBook;

    public GameObject ob_Enemy;
    public Text Player_Name;


    /*~----------------------------------/
    :                                    :
    :              能力値                :
    :                                    :
    /~----------------------------------*/
    // 全部エディターで入力
    public string CharaName;
    public int Lv;
    public int Hp;
    public int M_Hp;
    public int Mp;
    public int M_Mp;
    public int Str;
    public int Agi;
    public int Int;
    public int Vit;
    public int Luk;
    public int Cri;
    public int Fatigue;
    public StatusTable.Attribute Atr;

    // バトル中のステータス
    public int B_Str;
    public int B_Agi;
    public int B_Int;
    public int B_Vit;
    public int B_Luk;
    public int B_Cri;
    public StatusTable.Abnormality B_Abn;

    // 主人公、敵に適用
    public int Exp;
    // アレスタ、敵に適用
    public int G;

    /*~----------------------------------/
    :                                    :
    :        その他必要参照等            :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 自分がプレイヤーか敵か
    /// </summary>
    public Target PlyOrEnm;

    public string GroupName;

    /// <summary>
    /// オブジェクトのID
    /// </summary>
    public int ObjectID;

    /// <summary>
    /// 行動優先度
    /// </summary>
    public int B_Priority;

    /// <summary>
    /// 次の行動の種類
    /// </summary>
    public Act NextAct;

    /// <summary>
    /// 選択した魔法やスキル
    /// </summary>
    public int SelectedActNum;

    /// <summary>
    /// 敵、敵全体、プレイヤー、プレイヤー全体、自分自身か
    /// </summary>
    public Target TargetObject;

    /// <summary>
    /// 選択した敵、プレーヤー
    /// </summary>
    public int SelectedTgtNum;

    /// <summary>
    /// 選択した敵、プレーヤーの名前
    /// </summary>
    public string SelectedTgtName;

    /// <summary>
    /// 選択した敵の情報
    /// </summary>
    public List<Battle_Status> EnmStatus;

    /// <summary>
    /// ターンのはじめに選択したスキル
    /// </summary>
    public Skill SelectedSkl;

    /// <summary>
    /// ターンのはじめに選択したアイテム
    /// </summary>
    public Item SelectedItem;

    /// <summary>
    /// スキル、アイテムを使用したときのセリフ
    /// </summary>
    public string[] TalkText = new string[5];

    /// <summary>
    /// 行動
    /// </summary>
    public enum Act
    {
        Skill, Item, RunAway, PowerUp, Status, none = 0xff
    }

    /// <summay>
    /// 対象
    /// </summary>
    public enum Target
    {
        Player, Players, Enemy, Enemys, Myself
    }
    

    /// <summary>
    /// 初期化、出現時にBattleControlから呼び出し
    /// </summary>
    /// <param name="id"></param>
    public void StatusInit(int id, Target PlOrEn)
    {
        PlyOrEnm = PlOrEn;
        if (PlyOrEnm == Target.Enemy)
            GroupName = GameObject.Find("cdObject").GetComponent<CommonData>().BattleEnemyGroupName;
        else // Player
            GroupName = "こっち";
        ObjectID = id;

        B_Str = Str;
        B_Agi = Agi;
        B_Int = Int;
        B_Vit = Vit;
        B_Luk = Luk;
        B_Cri = Cri;
        B_Abn = StatusTable.Abnormality.Normal;

        Fatigue = 0;

        //Enemy_Name.text = Status.CharaName;
    }

    /// <summary>
    /// メニューバー表示、プレイヤーのみ使用可
    /// </summary>
    public void SetMenuBar()
    {
        if (ob_Menu == null)
        {
            ob_Menu = Instantiate(prefab_Menu, GameObject.Find("Panel").transform);
            GameObject.Find("BattleControl").GetComponent<BattleControl>().ob_Object.Add(ob_Menu);

            Menu = ob_Menu.GetComponent<Battle_MenuButton>();
            skillBook = GameObject.Find("SkillBook").GetComponent<SkillBook>();
            itemBook = GameObject.Find("ItemBook").GetComponent<ItemBook>();

            Menu.ObjectNum = GameObject.Find("BattleControl").GetComponent<BattleControl>().Player_Num;
            Menu.ObjectID = ObjectID;
            Menu.Phese = 0;
            Menu.isCanAct = true;
            skillBook.SetSkill(gameObject);
            itemBook.SetItem(gameObject);

        }
        else
            Menu.BattleUIShow();

    }

    /// <summary>
    /// 対象のステータスセット単体
    /// </summary>
    public void SetEnemyStatus(Battle_Status bs, List<Battle_Status> tgt_bss, int idx)
    {
        bs.EnmStatus.Clear();
        bs.EnmStatus.Add(tgt_bss[idx]);
    }

    /// <summary>
    /// 対象のステータスセット全体
    /// </summary>
    public void SetEnemyStatus(Battle_Status bs, List<Battle_Status> tgt_bss)
    {
        bs.EnmStatus.Clear();
        for (int i = 0; i < tgt_bss.Count; i++)
            bs.EnmStatus.Add(tgt_bss[i]);
    }
    
    /// <summary>
    /// Mp消費、Mp不足のときは威力1
    /// </summary>
    public void MpConsumption()
    {
        Mp -= SelectedSkl.mpCost;
        if (Mp < 0)
        {
            Mp = 0;
            SelectedSkl.effectAmount = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
