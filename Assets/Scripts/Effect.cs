﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {
    public GameObject ob_CameraEffectPlayer;
    public GameObject ob_CameraEffectEnemy;

    CommonData cd;
    public bool isEndRead = false;
    public EffekseerHandle handle;

    private void Start()
    {
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
        handle = new EffekseerHandle();
    }

    // Use this for initialization
    void Update ()
    {
        if (cd.isEndRead == true && this.isEndRead == false)
        {
            for (int i = 0; i < cd.skills.enm_skills.Count; i++)
                EffekseerSystem.LoadEffect(cd.skills.enm_skills[i].EffectName);
            for (int i = 0; i < cd.skills.ply_skills.Count; i++)
                EffekseerSystem.LoadEffect(cd.skills.ply_skills[i].EffectName);
            isEndRead = true;
        }
    }
	
    /// <summary>
    /// エフェクトの開始位置を設定
    /// 味方単体、味方全体、敵単体、敵全体、自分自身
    /// </summary>
    Vector3 EffectPositionSet(Battle_Status.Target tgt, int obnum, GameObject[] trs)
    {
        Vector3 vec = new Vector3();
        if (tgt == Battle_Status.Target.Player)
        {
            vec = trs[obnum].transform.position;
            vec.z = -9.0f;
        }
        if (tgt == Battle_Status.Target.Players)
        {
            vec = ob_CameraEffectPlayer.transform.position;
        }
        if (tgt == Battle_Status.Target.Enemy)
        {
            vec = trs[obnum].transform.position;
            vec.z = -9.0f;
        }
        if (tgt == Battle_Status.Target.Enemys)
        {
            vec = ob_CameraEffectEnemy.transform.position;
        }
        else
        {
            vec = trs[obnum].transform.position;
            vec.z = -9.0f;
        }

        return vec;
    }


    /// <summary>
    /// 発動エフェクト再生
    /// </summary>
	public void PlayActivateEffect(string efkname, GameObject[] trs, Battle_Status.Target tgt, Battle_Status.Target myself, int selectob)
    {
        if (tgt == Battle_Status.Target.Myself)
            tgt = myself;

        var vec = EffectPositionSet(tgt, selectob, trs);

        for (int i = 0; i < cd.skills.enm_skills.Count; i++)
        {
            if (efkname == cd.skills.enm_skills[i].EffectName)
            {
                handle = EffekseerSystem.PlayEffect(efkname, vec);
                return;
            }
        }
        for (int i = 0; i < cd.skills.ply_skills.Count; i++)
        {
            if (efkname == cd.skills.ply_skills[i].EffectName)
            {
                handle = EffekseerSystem.PlayEffect(efkname, vec);
                return;
            }
        }
    }

    
}
