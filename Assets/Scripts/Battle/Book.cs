﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book : MonoBehaviour {
    // TODO:最終的にはSkillとItemで互換させる
    protected PlayerController plycnt;
    protected Animator anim;
    protected Battle_Status bs;
    public Battle_MenuButton bmb;
    public Map_Action mapact;

    public delegate void EndProsess();
    public EndProsess ep;

    public void SetCallBack(EndProsess p)
    {
        ep = p;
    }

    protected void CallBack()
    {
        if (ep != null)
            ep.Invoke();

        ep = null;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
