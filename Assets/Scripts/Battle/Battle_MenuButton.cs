using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class Battle_MenuButton : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :            外部クラス	             :
    :                                    :
    /~----------------------------------*/
    public BattleControl btlctl;                // Start
	public PlayerController plycnt;             // Start
    public Battle_SelectedUILayerSort uisel;

    /*~----------------------------------/
    :                                    :
    :          ボタンとカーソル			 :
    :                                    :
    /~----------------------------------*/
    public AudioSource audioSource;
    public AudioClip audio_select;
    public AudioClip audio_decision;
    public AudioClip audio_cancel;
    
    private Animator anim_Menu;
    public GameObject[] ob_MainButtons = new GameObject[5];
    public Button[] btn_MainButtons = new Button[5];

    public SkillBook skillBook;
    public ItemBook itemBook;

    /*~----------------------------------/
    :                                    :
    :         変数とメソッド			 :
    :                                    :
    /~----------------------------------*/
    /// <summary> このオブジェクトの総数1 ~ 3 </summary>
    public int ObjectNum;

    /// <summary> このオブジェクトのID </summary>
    public int ObjectID;

    /// <summary> 選択処理のフェーズ、最初にボタンが押されると1、更に選択すると2、対象を選択すると3になる </summary>
    public int Phese = 0;

    /// <summary> 行動可能かどうか </summary>
    public bool isCanAct = true;

    /// <summary> カーソル位置 0 ~ 4 </summary>
    private int CursorLocation = 0;

    public enum SelectMenu
    {
        none, Skill, Item, Vitaryty, Magic
    }

    public SelectMenu selmenu = SelectMenu.none;

    /// <summary> 第一行動選択可 </summary>
    public bool isCanSelect = false;

    /// <summary> 第一行動選択完了 </summary>
    public bool isActSet = false;

    /// <summary> 第二行動選択完了 </summary>
    private bool isSecondActSet = false;

    /// <summary> 第三行動選択完了 </summary>
    private bool isThirdActSet = false;

    // Use this for initialization
    void Start ()
    {
		btlctl = GameObject.Find("BattleControl").GetComponent<BattleControl>();
		plycnt = GameObject.Find("cdObject").GetComponent<PlayerController>();
        anim_Menu = GetComponent<Animator>();

        plycnt.SetKeyDownMode(true);

        btn_MainButtons[0].onClick.AddListener(() => ClickEvent(0));
        btn_MainButtons[1].onClick.AddListener(() => ClickEvent(1));
        btn_MainButtons[2].onClick.AddListener(() => ClickEvent(2));
        btn_MainButtons[3].onClick.AddListener(() => ClickEvent(3));
        btn_MainButtons[4].onClick.AddListener(() => ClickEvent(4));
        btn_MainButtons[5].onClick.AddListener(() => ClickEvent(5));
        
        // パーティ人数による
        if (ObjectNum > 1)
        {
            btn_MainButtons[6].onClick.AddListener(() => ClickEvent(6));
            btn_MainButtons[7].onClick.AddListener(() => ClickEvent(7));
            btn_MainButtons[8].onClick.AddListener(() => ClickEvent(8));
        }
        else
        {
            ob_MainButtons[6].SetActive(false);
            ob_MainButtons[7].SetActive(false);
            ob_MainButtons[8].SetActive(false);
        }
        if (ObjectNum > 2)
        {
            btn_MainButtons[9].onClick.AddListener(() => ClickEvent(9));
            btn_MainButtons[10].onClick.AddListener(() => ClickEvent(10));
            btn_MainButtons[11].onClick.AddListener(() => ClickEvent(11));
        }
        else
        {
            ob_MainButtons[9].SetActive(false);
            ob_MainButtons[10].SetActive(false);
            ob_MainButtons[11].SetActive(false);
        }
        btn_MainButtons[12].onClick.AddListener(() => ClickEvent(12));

        skillBook = GameObject.Find("SkillBook").GetComponent<SkillBook>();
        itemBook = GameObject.Find("ItemBook").GetComponent<ItemBook>();
    }

    private void OnEnable()
    {
        if(plycnt != null)
            plycnt.SetKeyDownMode(true);
        uisel.UISortAsIndexFrontAndPlayAnime(CursorLocation);
        isCanSelect = false;
        StartCoroutine(ToSelectOn());
    }

    IEnumerator ToSelectOn()
    {
        yield return new WaitForSecondsRealtime(0.6f);
        isCanSelect = true;
        isActSet = false;
    }

    const int CursorWait = 10;
    int CursorWaitCount = 0;
    // Update is called once per frame
    /// <summary>
    /// カーソル移動
    /// </summary>
    void Update()
    {
        if (isCanSelect == false) return;

        var x = plycnt.Horizontal;
        var y = plycnt.Virtical;
        var enter = plycnt.isFire1;

        // print("入力" + x + "::" + y + " : " + CursorLocation);

        CursorMove(x, y);
        CursorButtonEvent(enter);
    }

    /// <summary>
    /// カーソル移動時のアニメーション
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    void CursorMove(float x, float y)
    {
        // 連続で流れるのを防止
        if (CursorWaitCount > 0)
        {
            CursorWaitCount--;
            return;
        }
        else
            CursorWaitCount = 0;


        if (x > 0 || y > 0)
        {
            CursorLocation++;
            audioSource.clip = audio_select;
            audioSource.Play();
        }
        else if (x < 0 || y < 0)
        {
            CursorLocation--;
            audioSource.clip = audio_select;
            audioSource.Play();
        }
        else
            return;

        if (CursorLocation < 0) CursorLocation = 12;
        else if (CursorLocation > 12) CursorLocation = 0;

        // パーティ人数による
        if (ObjectNum == 1)
        {
            if (CursorLocation == 11) CursorLocation = 5;
            else if (CursorLocation == 6) CursorLocation = 12;
        }
        else if (ObjectNum == 2)
        {
            if (CursorLocation == 11) CursorLocation = 8;
            else if (CursorLocation == 9) CursorLocation = 12;
        }

        if (isActSet == false)
        {
            uisel.UISortAsIndexFrontAndPlayAnime(CursorLocation);
            CursorWaitCount = (int)(Time.deltaTime * 60 * 5);
        }
    }
    

    void ClickEvent(int n)
    {
        if (isActSet) return;

        audioSource.clip = audio_decision;
        audioSource.Play();
        CursorLocation = n;
        uisel.UISortAsIndexFrontAndPlayAnime(CursorLocation);
        CursorWaitCount = (int)(Time.deltaTime * 60 * 5);
        CursorButtonEvent(true);
    }


    /// <summary>
    /// カーソルボタン
    /// </summary>
    void CursorButtonEvent(bool enter)
    {
        if (enter == false) return;
        plycnt.SetInputDelay();

        audioSource.clip = audio_decision;
        audioSource.Play();
        switch(CursorLocation)
        {
            case 0:
                RunAwayButtonEvent();
                break;
            case 1:
                SkillButtonEvent();
                break;
            case 2:
                ItemButtonEvent();
                break;
            case 3:
                VitaryStatusButtonEvent(1);
                break;
            case 4:
                MagicalStatusButtonEvent(1);
                break;
            case 5:
                PowerUpButtonEvent(1);
                break;
            case 6:
                VitaryStatusButtonEvent(2);
                break;
            case 7:
                MagicalStatusButtonEvent(2);
                break;
            case 8:
                PowerUpButtonEvent(2);
                break;
            case 9:
                VitaryStatusButtonEvent(3);
                break;
            case 10:
                MagicalStatusButtonEvent(3);
                break;
            case 11:
                PowerUpButtonEvent(3);
                break;
            case 12:
                CameraChange();
                break;
        }
    }

    /// <summary>
    /// スキルボタン
    /// </summary>
    void SkillButtonEvent()
    {
        uisel.UISortAsIndexFrontAndPlayAnime(1);
        if (isActSet == true) return;
        
        isActSet = true;
        isCanSelect = false;
        selmenu = SelectMenu.Skill;
        skillBook.SetCallBack(null);
        skillBook.ShowBook();

        print("Skill");
        return;
    }

    /// <summary>
    /// アイテムボタン
    /// </summary>
    void ItemButtonEvent()
    {
        uisel.UISortAsIndexFrontAndPlayAnime(2);
        if (isActSet == true) return;

        isActSet = true;
        isCanSelect = false;
        selmenu = SelectMenu.Item;
        itemBook.SetCallBack(null);
        itemBook.ShowBook();

        print("Item");
        return;
    }

    /// <summary>
    /// 逃走ボタン
    /// </summary>
    void RunAwayButtonEvent()
    {
        uisel.UISortAsIndexFrontAndPlayAnime(0);
        if (isActSet == true) return;

        // 行動のみセット
        isActSet = true;
        BattleUIHide();

        StartCoroutine(btlctl.TextMes.TextPlay(btlctl.Players[ObjectID].CharaName + "は逃げ出した！"));

        btlctl.Players[ObjectID].NextAct = Battle_Status.Act.RunAway;
        btlctl.Phese = BattleControl.ST.BATTLE;
        print("RunAway");
    }

    /// <summary>
    /// 覚醒ボタン
    /// </summary>
    void PowerUpButtonEvent(int n)
    {
        // TODO 今のところ実装なし
    }

    /// <summary>
    /// Hpステータス確認
    /// </summary>
    void VitaryStatusButtonEvent(int n)
    {
        if (isActSet == true) return;
        isActSet = true;
        Battle_Status bs = btlctl.Players[n - 1];
        string str = BattleControl.GetHpStr(bs);
        StartCoroutine(btlctl.TextMes.TextPlay(str));
        StartCoroutine(isActResetDelay());
    }

    /// <summary>
    /// Mpステータス確認
    /// </summary>
    void MagicalStatusButtonEvent(int n)
    {
        if (isActSet == true) return;
        isActSet = true;
        Battle_Status bs = btlctl.Players[n - 1];
        string str = BattleControl.GetMpStr(bs);
        StartCoroutine(btlctl.TextMes.TextPlay(str));
        StartCoroutine(isActResetDelay());
    }


    void CameraChange()
    {
        // TODO 今のところ何もなし
    }

    /// <summary>
    /// バトルメニューUIを表示
    /// </summary>
    public void BattleUIShow()
    {
        isActSet = false;
        isCanSelect = true;
        gameObject.SetActive(true);
        for (int i = 0; i < btn_MainButtons.Length; i++)
            btn_MainButtons[i].interactable = true;
        anim_Menu.Play("Battle_UI_Appear", 0);
    }

    /// <summary>
    /// バトルメニューUIを隠す
    /// </summary>
    public void BattleUIHide()
    {
        for (int i = 0; i < btn_MainButtons.Length; i++)
            btn_MainButtons[i].interactable = false;
        Invoke("Hide", 0.6f);
        anim_Menu.Play("Battle_UI_Remove", 0);
    }

    void Hide()
    {
        gameObject.SetActive(false);
    }

    IEnumerator isActResetDelay()
    {
        yield return new WaitForSecondsRealtime(0.8f);
        isActSet = false;
    }

}
