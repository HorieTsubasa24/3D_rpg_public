﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Map = MapWallChecker;

[System.Serializable]
public class MapTest : MonoBehaviour {
    // 全オブジェクト
    List<GameObject> MapObjects;

    // combine
    public GameObject ob_walls;
    public Combine combine;

    // combine以外
    public GameObject ob_wallsOther;

    /// <summary>
    /// 壁オブジェクトのプレハブ
    /// </summary>
    public GameObject[] prefab_Walls;

    /// <summary>
    /// 床オブジェクトのプレハブ
    /// </summary>
    public GameObject[] prefab_Tiles;

    /// <summary>
    /// 空間オブジェクトのプレハブ
    /// </summary>
    public GameObject[] prefab_Space;

    //マップのサイズなどの情報データ
    //データを参照する際は必ず使用
    public const int indexByte = 3;

    //マップデータ配列
    /*  test.byte
     *  
     *  indexByte(3Byte)
     *  mapData(100Byte)
     *  
     *  total 103Bytes
     */

    //イベントマップデータ配列
    /*  test.byte
     *  
     *  indexByte(2Byte)
     *  iventmapData(100Byte)
     *  
     *  total 103Bytes
     */
     

    // このマップのフロア
    public int Map_Floor;

    //マップサイズ。
    public Vector2 Map_Size;
    
    //縦壁マップデータ格納配列
    public List<Byte> wallDatas1;

    //横壁マップデータ格納配列
    public List<Byte> wallDatas2;

    //床データ1格納配列
    public List<Byte> floorDatas1;

    //床データ2格納配列
    public List<Byte> floorDatas2;
    
    //踏破したかしていないか
    public List<Byte> isStepfloors;

    //イベントマップデータ格納配列
    List<Byte> iv_buf;


    //位置イベント、ギミックイベント
    // 保留
    

    // イベントデータ取得
    public List<Byte> GetIventMapData()
    {
        return iv_buf;
    }

    // Use this for initialization
    void Start()
    {
        print("起動");

        MapObjects = new List<GameObject>();

        //読み込み
        wallDatas1 = new List<byte>();
        wallDatas2 = new List<byte>();
        iv_buf = new List<byte>();
        
        MapLoad();
    }
	
    void MapLoad()
    {
        ReadFile("F1");

        //読み込み終了後、カメラオブジェクト群の有効化
        GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);

        //セット
        print(Map_Floor);
        print("マップサイズ:" + Map_Size);
        SetMap();
    }

    // Update is called once per frame
    void Update () {
		
	}

    int a = 0;

    //読み込み関数
    bool ReadFile(string filename)
    {
        //ファイル情報取得
        FileStream map = new FileStream(Application.dataPath + "/StreamingAssets/MapData/Map/" + filename + ".txt", FileMode.Open, FileAccess.Read);
        FileStream iv_map = new FileStream(Application.dataPath + "/StreamingAssets/MapData/Map/iv_" + filename + ".byte", FileMode.Open, FileAccess.Read);
        try
        {

            // フロアマップ
            using (StreamReader sr = new StreamReader(map))
            {
                while (!sr.EndOfStream)
                {
                    //ファイルが空の場合
                    if (map.Length == 0)
                        return false;

                    // マップ情報
                    string str = sr.ReadLine();
                    var isHeadData = FloorInfoSet(str);
                    if (isHeadData == true) continue;

                    // マップロード
                    a = 0;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x + 1, wallDatas1);   // 縦マップ
                    a = 1;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y + 1, (int)Map_Size.x, wallDatas2);   // 横マップ
                    a = 2;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x, floorDatas1);      // フロア床
                    a = 3;
                    str = ReadRepeatRowsMapData(str, sr, (int)Map_Size.y, (int)Map_Size.x, floorDatas2);      // フロア空間

                    // TODO 踏破リスト初期化はまた正式に作る。
                    for (int i = 0; i < (int)(Map_Size.x * Map_Size.y); i++)
                        isStepfloors.Add(0);
                    break;
                }
            }
        }
        catch
        {
            print("読み込めませんでした。:" + a.ToString());
        }


        try { 
        // イベント(フォーマットはイベントマトリクスのみでフロア情報は含まない)
        using (BinaryReader iv_br = new BinaryReader(iv_map))
            {
                //ファイルが空の場合
                if (iv_map.Length == 0)
                    return false;

                //イベントバイナリ読み込み
                int i = 0;
                while (i < iv_map.Length)
                {
                    //List<Byte>に追加
                    iv_buf.Add(iv_br.ReadByte());
                    i++;
                }

                //読み込み終了後、カメラオブジェクト群の有効化
                GameObject.Find("Player").transform.Find("CameraMoveObject").gameObject.SetActive(true);
            }

        }
        catch
        {
            print("読み込めませんでした。" + "イベント");
        }

        return true;
    }
    
    bool FloorInfoSet(string str)
    {
        string[] reserved = { "MapName", "FloorName", "Floor", "MapSizeX", "MapSizeY", "[MapData]" };

        for (int i = 0; i < reserved.Length; i++)
        {
            if (str.IndexOf(reserved[i]) != -1)
            {
                FloorInfoSetOfMap(i, str);
                return true;
            }
        }
        return false;
    }
    
    void FloorInfoSetOfMap(int i, string str)
    {
        int n = 0;
        switch (i)
        {
            // 今のフロアセット
            case 2:
                n = str.IndexOf('=');
                n += 2;
                string flr = "";
                flr += str[n];
                if (n < str.Length - 1) flr += str[n + 1];
                
                Map_Floor = int.Parse(flr);
                break;
            // 横幅
            case 3:
                n = str.IndexOf('=');
                n += 2;
                string x = "";
                x += str[n];
                if (n < str.Length - 1) x += str[n + 1];

                Map_Size.x = int.Parse(x) + 1;
                break;
            // 横幅
            case 4:
                n = str.IndexOf('=');
                n += 2;
                string y = "";
                y += str[n];
                if (n < str.Length - 1) y += str[n + 1];

                Map_Size.y = int.Parse(y) + 1;
                break;
            default:
                break;
        }
    }

    string ReadRepeatRowsMapData(string str, StreamReader sr, int n, int m, List<byte> list)
    {
        for (int j = 0; j < n; j++)
        {
            for (int i = 0; i < m * 2; i += 2)
            {
                string s1 = str[i].ToString() + str[i + 1].ToString();
                Byte value = (Byte)Convert.ToInt32(s1, 16);
                list.Add(value);
            }
            str = sr.ReadLine();
        }
        return str;
    }

    //walldatasのデータをもとに壁を生成
    void SetMap()
    {
        // マップオブジェクト初期化
        DeleteMapObject();
        // 縦壁
        ArrangementOfMapObjects((int)Map_Size.x + 1, (int)Map_Size.y, wallDatas1, 0);
        // 横壁
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y + 1, wallDatas2, 1);
        // 床
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas1, 2);
        // 空間
        //ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas2, 3);
        // 天井
        ArrangementOfMapObjects((int)Map_Size.x, (int)Map_Size.y, floorDatas1, 20);

    }

    /// <summary>
    /// マップ物の配置
    /// </summary>
    /// <param name="x">横幅</param>
    /// <param name="y">縦幅</param>
    /// <param name="list">データ</param>
    /// <param name="i">データの参照段落(0 ~ 3)</param>
    void ArrangementOfMapObjects(int x, int y, List<byte> list, int like)
    {
        for(int h = 0; h < y; h++)
        {
            string n = "";
            for (int k = 0; k < x; k++)
            {
                n += list[k + h * x].ToString("X");
                //1マスの壁データ
                Byte objectData = list[k + h * x];
                WallSet(k, -h, like, objectData);
            }
            print(n);
        }
        
    }

    /// <summary>
    /// 各種プレハブオブジェクトの取得
    /// </summary>
    /// <returns></returns>
    GameObject GetSetObject(int like, int data)
    {
        if (like == 0 || like == 1)
        {
            return prefab_Walls[data];
        }
        else if (like == 2)
        {
            return prefab_Tiles[data];
        }
        else if (like == 3)
        {
            return prefab_Space[data];
        }
        else // like == 20
            return prefab_Walls[0];
    }

    // オブジェクトのセット
    void WallSet(int x, int y, int like, Byte w_Data)
    {
        var vec = Map.GetWallVector(x, y, w_Data, like);
        var quat = Map.GetWallQuat(x, y, w_Data, like);
        GameObject setobject;
        setobject = GetSetObject(like, w_Data);

        if (setobject == null) return;

        // 普通の壁はメッシュ結合
        if (like < 2 && w_Data == 01)
        {
            var a = Instantiate(setobject, vec, quat, ob_walls.transform);
            MapObjects.Add(a);
            combine.MeshCombine(a);
        }
        else if (like >= 2 || (like < 2 && w_Data != 00))
        {
            var b = Instantiate(setobject, vec, quat, ob_wallsOther.transform);
            MapObjects.Add(b);
        }
        return;
    }
    
    // マップオブジェクト全削除
    public void DeleteMapObject()
    {
        if (MapObjects.Count == 0) return;
        foreach(var a in MapObjects)
        {
            Destroy(a);
        }

        MapObjects.Clear();
    }


    /*********************************
     * 
     *          マップの判定
     * 
     * *******************************/
     /// <summary>
     /// 前に壁があるか？
     /// </summary>
     /// <param name="po"></param>
     /// <returns></returns>
    public int WallInFront(CommonData.Position po)
    {
        var pos = WallRefSet(po);
        var a = CheckisWallInFrontFromData(pos.x, pos.y, pos.direction);
        return a;
    }

    CommonData.Position WallRefSet(CommonData.Position po)
    {
        var pos = new CommonData.Position(po.x, po.y, po.direction, po.floor);
        switch (pos.direction)
        {
            case 0:
                break;
            case 1:
                pos.x = pos.x + 1;
                break;
            case 2:
                pos.y = pos.y + 1;
                break;
            default://d = 3
                break;
        }
        return pos;
    }

    int CheckisWallInFrontFromData(int x, int y, int d)
    {
        if (d == 0 || d == 2)
        {
            return wallDatas2[x + y * (int)Map_Size.x];
        }
        else
        {
            return wallDatas1[x + y * ((int)Map_Size.x + 1)];
        }
    }
    
}
