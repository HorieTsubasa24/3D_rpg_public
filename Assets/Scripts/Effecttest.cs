﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effecttest : MonoBehaviour {
    public GameObject ob;
    Vector3 vec;
    Effect efk;
    CommonData cd;
    int idx = 0;

	// Use this for initialization
	void Start () {
        efk = GetComponent<Effect>();
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
        vec = ob.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        try
        {
            if (efk.isEndRead && efk.handle.exists == false)
            {
                efk.handle = EffekseerSystem.PlayEffect(cd.skills.enm_skills[idx].EffectName, vec);
                idx = (idx < cd.skills.enm_skills.Count) ? idx + 1 : 0;
            }
        }
        catch
        {
            print("idx: " + idx + ", skillCount: " + cd.skills.enm_skills.Count );
            idx = 0;
        }
    }
}
