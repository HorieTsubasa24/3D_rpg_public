﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneGotoOther : MonoBehaviour {
    public CommonData cd;

    // Update is called once per frame
    void Update () {
		if (cd != null && cd.isEndRead == true)
            SceneManager.LoadScene("Map");

    }

    public void FindCd()
    {
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
    }
}
