﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerData 
{
    public bool isEndRead = false;

    public PlayerData()
    {
        Ply_st = new StatusClass();
        isEndRead = true;
    }

    /// <summary>
    /// 仲間にいるか？
    /// </summary>
    public bool isJoined;

    /// <summary>
    /// 名前
    /// </summary>
    public string PlayerName;

    /// <summary>
    /// プレイヤーの能力
    /// </summary>
    public StatusClass Ply_st;

    // Ply_stの外からアクセスする用
    /// <summary>レベル</summary>
    public int LV
    {
        get { return Ply_st.Lv; }
        set { Ply_st.Lv = value; }
    }
    /// <summary>Hp</summary>
    public int HP
    {
        get { return Ply_st.Hp; } set { Ply_st.Hp = value; }
    }
    /// <summary>最大Hp</summary>
    public int M_HP
    {
        get { return Ply_st.M_Hp; }
        set { Ply_st.M_Hp = value; }
    }
    /// <summary>Mp</summary>
    public int MP
    {
        get { return Ply_st.Mp; }
        set { Ply_st.Mp = value; }
    }
    /// <summary>最大Mp</summary>
    public int M_MP
    {
        get { return Ply_st.M_Mp; }
        set { Ply_st.M_Mp = value; }
    }
    /// <summary>魔力</summary>
    public int STR
    {
        get { return Ply_st.Str; }
        set { Ply_st.Str = value; }
    }
    /// <summary>素早さ</summary>
    public int AGI
    {
        get { return Ply_st.Agi; }
        set { Ply_st.Agi = value; }
    }
    /// <summary>賢さ</summary>
    public int INT
    {
        get { return Ply_st.Int; }
        set { Ply_st.Int = value; }
    }
    /// <summary>元気度</summary>
    public int VIT
    {
        get { return Ply_st.Vit; }
        set { Ply_st.Vit = value; }
    }
    /// <summary>運のよさ</summary>
    public int LUK
    {
        get { return Ply_st.Luk; }
        set { Ply_st.Luk = value; }
    }
    /// <summary>クリティカル度</summary>
    public int CRI
    {
        get { return Ply_st.Cri; }
        set { Ply_st.Cri = value; }
    }
    /// <summary>属性</summary>
    public StatusTable.Attribute ATR
    {
        get { return Ply_st.Atr; }
        set {
            if (value.GetTypeCode() == TypeCode.Int32) Ply_st.Atr = (StatusTable.Attribute)value;
            else Ply_st.Atr = value;
        }
    }
    /// <summary>経験値</summary>
    public int EXP
    {
        get { return Ply_st.Exp; }
        set { Ply_st.Exp = value; }
    }
    /// <summary>現レベルでの経験値の底</summary>
    public int EXP_BTM
    {
        get { return Ply_st.Exp_buttom; }
        set { Ply_st.Exp_buttom = value; }
    }
    /// <summary>現レベルでの経験値の上限</summary>
    public int EXP_UPR
    {
        get { return Ply_st.Exp_upper; }
        set { Ply_st.Exp_upper = value; }
    }
    /// <summary>所持金(アレスタのみ)</summary>
    public int G
    {
        get { return Ply_st.G; }
        set { Ply_st.G = value; }
    }
    /// <summary>疲労度</summary>
    public int Fatigue
    {
        get { return Ply_st.Fatigue; }
        set { Ply_st.Fatigue = value; }
    }


    const int NumSkill = 12;
    /// <summary>
    /// プレイヤーのスキル
    /// </summary>
    public Skill[] Ply_skl = new Skill[NumSkill];

    const int NumItem = 12;
    /// <summary>
    /// プレイヤーのアイテム
    /// </summary>
    public Item[] Ply_item = new Item[NumItem];
}


/// <summary>
/// 能力値クラス
/// 能力、属性
/// </summary>
[System.Serializable]
public class StatusClass
{
    // 能力
    public int Lv;
    public int Hp;
    public int M_Hp;
    public int Mp;
    public int M_Mp;
    public int Str;
    public int Agi;
    public int Int;
    public int Vit;
    public int Luk;
    public int Cri;
    public int Fatigue;
    public StatusTable.Attribute Atr;
    
    public int B_Str;
    public int B_Agi;
    public int B_Int;
    public int B_Vit;
    public int B_Luk;
    public int B_Cri;

    // 主人公、敵に適用
    // 累積値
    public int Exp;
    // 現在レベルの底値
    public int Exp_buttom;
    // 現在レベルの上限値
    public int Exp_upper;
    // レベルごとの上限値
    public int[] Exps;
    // アレスタ、敵に適用
    public int G;
}

/*
 * 能力値の説明

全てintに収まりさえすればよい、わかりやすさのため0~255の値を取る。
・属性5つ(火→氷→地→雷→風→火(→は右の属性に対して強いことを表す。))
・LV(0 ~ 63)
・HP(ファジーパラメーターで表示)
・M_HP(マックス数値)
・MP(ファジーパラメーターで表示)
・M_MP(マックス数値)
・STR	力　　　魔法攻撃で相手に与えるダメージの基本値に影響
・AGI	素早さ	先制攻撃や相手の攻撃をよける確率に影響
・INT	賢さ	魔法攻撃ダメージや魔法攻撃の被ダメ軽減に影響
・VIT	元気度	被ダメ軽減に影響
・LUK	運	アイテムドロップ率や攻撃を避ける確率に影響
・CRI	技力	クリティカルヒット成功率に影響
・Fatigue 疲労度　0 ~ 100 、0のときに覚醒コマンドを実行できる。
　　　　　　　　　また、疲労度が大きいと能力が少し下がる。
　　　　　　　　　

・B_STR　バトル時のSTR(バトルが終わればもとに戻る)
・B_AGI　バトル時のAGI
・B_INT　バトル時のINT
・B_VIT　バトル時のVIT
・B_LUK　バトル時のLUK
・B_CRI　バトル時のCRI

敵キャラ独自のパラメーター
・EXP　経験値　キャラクターの持つ経験値
・G　お金　キャラクターの持つお金
 */