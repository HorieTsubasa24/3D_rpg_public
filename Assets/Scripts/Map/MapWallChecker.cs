﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class MapWallChecker
{
    /*壁のタイプ
    *  1:壁
    *  2:扉
    *  3:鍵付きの鍵
    *  4:隠し扉
    *  5:見えない扉
    *  6:一方通行の壁1(右)
    *  7:一方通行の壁2(左)
    *  8:一方通行の扉1(右)
    *  9:一方通行の扉2(左)
    *  A:看板1(右)
    *  B:看板2(左)
    *  C:スイッチ(右)
    *  D:スイッチ(左)
    *  E:罠
    *  F:カンバン両面
    * 10:スイッチ両面
    */

    // オブジェクトごとの位置ずれに対応
    static Vector3[] WallVec = {
        new Vector3(),               //  0:
        new Vector3(0, 0, 0),        //  1:壁
        new Vector3(0, 0.11f, 0),    //  2:扉
        new Vector3(),               //  3:鍵付きの鍵
        new Vector3(),               //  4:隠し扉
        new Vector3(),               //  5:見えない扉
        new Vector3(),               //  6:一方通行の壁1(右)
        new Vector3(),               //  7:一方通行の壁2(左)
        new Vector3(),               //  8:一方通行の扉1(右)
        new Vector3(),               //  9:一方通行の扉2(左)
        new Vector3(),               //  A:看板1(右)
        new Vector3(),               //  B:看板2(左)
        new Vector3(),               //  C:スイッチ(右)
        new Vector3(),               //  D:スイッチ(左)
        new Vector3(),               //  E:罠
        new Vector3(),               //  F:カンバン両面
        new Vector3()                //  10:スイッチ両面
};

    // オブジェクトごとの回転ずれに対応
    static Vector3[] WallQuat = {
        new Vector3(0, 0, 0),       //  0: 
        new Vector3(0, 0, 0),       //  1:壁
        new Vector3(-90f, 0, 0),    //  2:扉
        new Vector3(),              //  3:鍵付きの鍵
        new Vector3(),              //  4:隠し扉
        new Vector3(),              //  5:見えない扉
        new Vector3(),              //  6:一方通行の壁1(右)
        new Vector3(),              //  7:一方通行の壁2(左)
        new Vector3(),              //  8:一方通行の扉1(右)
        new Vector3(),              //  9:一方通行の扉2(左)
        new Vector3(),              //  A:看板1(右)
        new Vector3(),              //  B:看板2(左)
        new Vector3(),              //  C:スイッチ(右)
        new Vector3(),              //  D:スイッチ(左)
        new Vector3(),              //  E:罠
        new Vector3(),              //  F:カンバン両面
        new Vector3()               //  10:スイッチ両面
    };

    /*  ----床----
    *  0:床
    *  1:上り階段
    *  2:下り階段
    *  3:エレベータ
    *  4:回転床
    *  5:罠
    *  6:シュート
    *  7:宿屋
    *  8:武器屋
    *  9:防具屋
    *  A:アイテム屋
    *  B:寺院
    *  C:体力回復の泉
    *  D:魔力回復の泉
    *  E:強制転送ワープ
    *  F:
    */

    // オブジェクトごとの位置ずれに対応
    static Vector3[] TileVec = {
        new Vector3(),               //  0:床
        new Vector3(0, 0, 0),        //  1:上り階段
        new Vector3(0, 0, 0),        //  2:下り階段
        new Vector3(),               //  3:エレベータ
        new Vector3(0, 0, 0),        //  4:回転床
        new Vector3(0, 0, 0),        //  5:罠
        new Vector3(),               //  6:シュート
        new Vector3(0, 0, 0),        //  7:宿屋
        new Vector3(0, 0, 0),        //  8:武器屋
        new Vector3(),               //  9:防具屋
        new Vector3(0, 0, 0),        //  A:アイテム屋
        new Vector3(0, 0, 0),        //  B:寺院
        new Vector3(),               //  C:体力回復の泉
        new Vector3(0, 0, 0),        //  D:魔力回復の泉
        new Vector3(0, 0, 0),        //  E:強制転送ワープ
        new Vector3(),               //  F:床
    };

    // オブジェクトごとの回転ずれに対応
    static Vector3[] TileQuat = {
        new Vector3(),               //  0:床
        new Vector3(0, 0, 0),        //  1:上り階段
        new Vector3(0, 0, 0),        //  2:下り階段
        new Vector3(),               //  3:エレベータ
        new Vector3(0, 0, 0),        //  4:回転床
        new Vector3(0, 0, 0),        //  5:罠
        new Vector3(),               //  6:シュート
        new Vector3(0, 0, 0),        //  7:宿屋
        new Vector3(0, 0, 0),        //  8:武器屋
        new Vector3(),               //  9:防具屋
        new Vector3(0, 0, 0),        //  A:アイテム屋
        new Vector3(0, 0, 0),        //  B:寺院
        new Vector3(),               //  C:体力回復の泉
        new Vector3(0, 0, 0),        //  D:魔力回復の泉
        new Vector3(0, 0, 0),        //  E:強制転送ワープ
        new Vector3(),               //  F:床
    };
    /*  ----空間----
    *  1:ダークゾーン
    *  2:魔封じ
    *  3:ダメージ
    *  4:気になるポイント
    *  5:戦闘開始
    *  
    *  ----物----
    *  (イベント)
    */

    // 空間ごとの位置ずれに対応
    static Vector3[] SpaceVec = {
        new Vector3(),               //  0:床
        new Vector3(0, 0, 0),        //  1:上り階段
        new Vector3(0, 0, 0),        //  2:下り階段
        new Vector3(),               //  3:ダメージ
        new Vector3(0, 0, 0),        //  4:気になるポイント
        new Vector3(0, 0, 0),        //  5:戦闘開始
    };

    // オブジェクトごとの回転ずれに対応
    static Vector3[] SpaceQuat = {
        new Vector3(),               //  0:床
        new Vector3(0, 0, 0),        //  1:上り階段
        new Vector3(0, 0, 0),        //  2:下り階段
        new Vector3(),               //  3:ダメージ
        new Vector3(0, 0, 0),        //  4:気になるポイント
        new Vector3(0, 0, 0),        //  5:戦闘開始
    };

    public static Vector3 GetWallVector(int x, int y, Byte data, int like)
    {
        Vector3 vo = new Vector3(x, 0, y);
        // TODO 配列を全通り埋める
        //if (data > 1) data = 0;
        switch (like)
        {
            // 縦壁
            case 0:
                Vector3 wv0 = WallVec[data];
                return wv0 + new Vector3(-0.5f, 0, 0) + vo;
            // 横壁
            case 1:
                Vector3 wv1 = WallVec[data];
                return wv1 + new Vector3(0, 0, 0.5f) + vo;
            // 床
            case 2:
                Vector3 tv0 = TileVec[data];
                return tv0 + new Vector3(0, -0.5f, 0) + vo;
            // 空間
            case 3:
                Vector3 sv = SpaceVec[data];
                return sv + new Vector3(0, -0.5f, 0) + vo;
            // 天井
            case 20:
                Vector3 tv1 = SpaceVec[data];
                return tv1 + new Vector3(0, 0.5f, 0) + vo;
            default:
                return new Vector3() + vo;
        }
    }

    public static Quaternion GetWallQuat(int x, int y, Byte data, int like)
    {
        // TODO 配列を全通り埋める
        //if (data > 1) data = 0;
        switch (like)
        {
            // 縦壁
            case 0:
                var wq = WallQuat[data];
                return Quaternion.Euler(0 + wq.x, -90f + wq.y, 0 + wq.z);
            // 横壁
            case 1:
                var wwq = WallQuat[data];
                return Quaternion.Euler(0 + wwq.x, 0 + wwq.y, 0 + wwq.z);
            // 床
            case 2:
                var tq = WallQuat[data];
                return Quaternion.Euler(90f + tq.x, 0 + tq.y, 0 + tq.z);
            // 空間
            case 3:
                var sq = WallQuat[data];
                return Quaternion.Euler(90f + sq.x, 0 + sq.y, 0 + sq.z);
            // 天井
            case 20:
                var ttq = WallQuat[data];
                return Quaternion.Euler(90f + ttq.x, 0 + ttq.y, 0 + ttq.z);
            default:
                return Quaternion.Euler(0, 0, 0);
        }
    }

}
