﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    /*-----------------------------------
	 * 									*
	 * 				入力許可				*
	 * 									*
	-----------------------------------*/
    /// <summary>
    /// 入力許可
    /// </summary>
    public bool isInputAllow;
    /// <summary>
    /// キャンセルキー メニュー開閉ボタン使用可能
    /// </summary>
    public bool isMenuAllow;
    /// <summary>
    /// 移動ボタン使用可能
    /// </summary>
    public bool isMoveAllow;
    /// <summary>
    /// 文字列表示中
    /// 外部のメソッドで変化する。
    /// </summary>
    public bool isPlayString;
    /// <summary>
    /// trueでキーダウンで読み込み
    /// falseで押しっぱなしで読み込み
    /// </summary>
    public bool isKeyDownMode = true;
	/// <summary>
	/// ディレイ
	/// </summary>
	private int delay = 0;
	private readonly int delayflame = 30;

	/*-----------------------------------
	 * 									*
	 * 		ジョイスティックの入力バッファ	*
	 * 									*
	-----------------------------------*/
	public bool[] joy_buttons_buf = new bool[3];
	public Vector2 joy_Axis_buf;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < joy_buttons_buf.Length; i++)
			joy_buttons_buf[i] = false;
		joy_Axis_buf = new Vector2();
		isInputAllow = true;
		isPlayString = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (delay > 0) { delay--; return; }

        for (int i = 0; i < joy_buttons_buf.Length; i++)
        {
            if (isKeyDownMode)
                joy_buttons_buf[i] = Input.GetButtonDown("Fire" + (1 + i).ToString());
            else
                joy_buttons_buf[i] = Input.GetButton("Fire" + (1 + i).ToString());
        }

		joy_Axis_buf.x = Input.GetAxis("Horizontal");
		joy_Axis_buf.y = Input.GetAxis("Vertical");
	}

    public void SetKeyDownMode(bool b)
    {
        isKeyDownMode = b;
    }

	public void SetInputDelay()
	{
		delay = (int)(delayflame * Time.deltaTime * 60);
		for (int i = 0; i < joy_buttons_buf.Length; i++)
			joy_buttons_buf[i] = false;
		joy_Axis_buf.x = 0.0f;
		joy_Axis_buf.y = 0.0f;
	}

	public void SetAllow(bool a)
	{
		isInputAllow = a;
	}

	public bool isFire1
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[0]; }
	}
	public bool isFire2
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[1]; }
	}
	public bool isFire3
	{
		get { return (isInputAllow == false) ? false : joy_buttons_buf[2]; }
	}
	public float Horizontal
	{
		get { return (isInputAllow == false) ? 0.0f : joy_Axis_buf.x; }
	}
	public float Virtical
	{
		get { return (isInputAllow == false) ? 0.0f : joy_Axis_buf.y;}
	}
    /// <summary>
    /// ボタンや方向キーに触れているか
    /// </summary>
    public bool IsButtonAndAxisIsNoZero
    {
        get
        {
            return (isFire1 || isFire2 || isFire3 || (Horizontal != 0.0f) || (Virtical != 0.0f));
        }
    }

}
