﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StableAspect : MonoBehaviour {

    private Camera cam;

    // カメラのサイズ
    private float wid = 1920;
    private float hei = 1080;

    private float pixcelPerUnit = 50f;


    private void Update()
    {
        float aspect = (float)Screen.height / (float)Screen.width;
        float bgAspect = hei / wid;

        // カメ取得、サイズ設定
        cam = GetComponent<Camera>();
        cam.orthographicSize = (hei / 2f / pixcelPerUnit);

        // アスペクトセット
        if (bgAspect > aspect)
        {
            // 倍率 * Screen.width で大きさセット
            float bgScale = hei / Screen.height;

            float camWid = wid / (Screen.width * bgScale);

            cam.rect = new Rect((1f - camWid) / 2, 0f, camWid, 1f);
        }
        else
        {
            // 倍率 * Screen.width で大きさセット
            float bgScale = wid / Screen.width;

            float camHei = hei / (Screen.height * bgScale);

            cam.rect = new Rect((1f - camHei) / 2, 0f, camHei, 1f);
        }

    }

}
