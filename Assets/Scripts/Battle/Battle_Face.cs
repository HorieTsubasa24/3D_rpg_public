﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_Face : FaceStatus {
    public Animator anim;
    public Battle_Status bs;

	// Use this for initialization
	override public void Start () {
        anim = GetComponent<Animator>();
        bs = GetComponent<Battle_Status>();
        befHp = 0;
        nowHp = bs.Hp;
	}

    public float befHp;
    public float nowHp;
    // Update is called once per frame
    override public void Update () {
        if (befHp != nowHp || anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") == true)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Damage") == true ||
                anim.GetCurrentAnimatorStateInfo(0).IsName("Down"))
                return;

            int a = CharacterFaceSet(bs.M_Hp, bs.Hp);
            switch(a)
            {
                case 0:
                    anim.Play("Down", 0);
                    break;
                case 1:
                    anim.Play("Idle4", 0);
                    break;
                case 2:
                    anim.Play("Idle3", 0);
                    break;
                case 3:
                    anim.Play("Idle2", 0);
                    break;
                case 4:
                    anim.Play("Idle1", 0);
                    break;
            }
        }
        befHp = nowHp;
        nowHp = bs.Hp;
    }
}
