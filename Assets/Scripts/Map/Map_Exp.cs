﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map_Exp : MonoBehaviour {
    public int PlayerID = 0;        // 0 ~ 2
    public Slider slider;
    public CommonData cd;
    float Exp { get { return cd.PlayerData[PlayerID].EXP - cd.PlayerData[PlayerID].EXP_BTM; } }
    float max_Exp { get { return cd.PlayerData[PlayerID].EXP_UPR - cd.PlayerData[PlayerID].EXP_BTM; } }

	// Use this for initialization
	void Start () {
        slider = GetComponent<Slider>();
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
    }

    // Update is called once per frame
    void Update()
    {
        ExpValueChange();
    }


    public void ExpValueChange()
    {
        if (slider.maxValue != max_Exp)
            slider.maxValue = max_Exp;


        if (slider.value != Exp)
        {
            if (Exp > slider.value)
            {
                slider.value++;
            }
            if (Exp < slider.value)
            {
                slider.value--;
            }
        }
    }
    
}
