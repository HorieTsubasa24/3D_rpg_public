﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[System.Serializable]
public class CameraMove : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :        外部のクラス                :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// マップ
    /// </summary>
    public MapTest map;

    /// <summary>
    /// マップで再生する効果音
    /// </summary>
    public Map_Sound mapsound;

    /// <summary>
    /// イベントシステム
    /// </summary>
    MapEventSystem iv_sys;

    /// <summary>
    /// データクラス
    /// </summary>
    CommonData cd;

    /// <summary>
    /// コントローラー
    /// </summary>
    PlayerController plycnt;

    /*~----------------------------------/
    :                                    :
    :    内部で使うメソッドと変数        :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 起動初めの時間(連続エンカウント回避)
    /// </summary>
    private int initCount;

    /// <summary>
    /// 移動にかかる時間
    /// </summary>
    public const int MOVE_COUNT = 15;

    /// <summary>
    /// 移動カウント
    /// </summary>
    public int Move_count = MOVE_COUNT;

    /// <summary>
    /// 前フレーム方向キーが押されたか
    /// </summary>
    bool BEFORE_KEY = true;
    
    /// <summary>
    /// 移動する向き
    /// </summary>
    Arrow Move_Arrow = Arrow.none;


    enum Arrow
    {
        none, up, right, down, left, wall
    }

    /*~----------------------------------/
    :                                    :
    :        メインのメソッド            :
    :                                    :
    /~----------------------------------*/
    // Update is called once per frame
    void FixedUpdate()
    {
        // 起動するときの処理
        if (initCount > 0)
        {
            initCount--;
            if (initCount == 0)
            {
                var value = cd.TransformToNowPositionSet(gameObject);
                cd.intPos.LocationSet((int)value.x, (int)value.y, (int)value.z);
                BeforePositionSet(cd.intPos);
            }
            return;
        }

        // ギミックシステム

        // イベントシステム
        // バトルシステム
        // 静止している状態で呼び出し
        if (Move_count == 0)
        {
            iv_sys.IventCheck();
        }

        BeforeFlamePositionSet(cd.intPos);

        // キー入力
        // 壁も判定
        InputMoveRequest(plycnt.isMoveAllow);

        // 歩く
        Move(plycnt.isMoveAllow);
    }

    /// <summary>
    /// 移動(歩き)
    /// </summary>
    void Move(bool MP)
    {
        // 移動許可がない
        if (MP == false)
        {
            // 方向解除
            Move_Arrow = Arrow.none;

            // 移動カウントゼロ
            Move_count = 0;
            return;
        }

        if (Move_count == 1)
            // 前回の座標に今の座標をセット
            BeforePositionSet(cd.intPos);
        if (Move_count == 0)
        {
            // 方向解除
            Move_Arrow = Arrow.none;

            BEFORE_KEY = false;

            // イベント、バトル用の1フレーム前の座標
            BeforeFlamePositionSet(cd.intPos);
            cd.BeforeFlamePos = new CommonData.Position(cd.intPos.x, cd.intPos.y, cd.intPos.direction, cd.intPos.floor);

            var val = cd.TransformToNowPositionSet(gameObject);
            
            // 外部参照用座標に代入
            cd.intPos.LocationSet((int)val.x, (int)val.y, (int)val.z);

            // メニュー許可
            plycnt.isMenuAllow =　true;

            return;
        }
        
        // セットされた方向に1フレームずつ移動
        switch (Move_Arrow)
        {
            case Arrow.up:
                // 足音
                if (Move_count == MOVE_COUNT / 3)
                    mapsound.WalkSoundPlay();
                gameObject.transform.Translate(0, 0, 1.0f / MOVE_COUNT);
                break;
            case Arrow.right:
                gameObject.transform.Rotate(0, 90f / MOVE_COUNT, 0);
                break;
            case Arrow.down:
                gameObject.transform.Rotate(0, 180f / MOVE_COUNT, 0);
                break;
            case Arrow.left:
                gameObject.transform.Rotate(0, -90f / MOVE_COUNT, 0);
                break;
            case Arrow.wall:
                if (Move_count >= (MOVE_COUNT / 3))
                {
                    gameObject.transform.Translate(0, 0, 1.0f / MOVE_COUNT);
                    if (Move_count == (MOVE_COUNT / 3))
                        mapsound.WallHitPlay();
                }
                else
                    gameObject.transform.Translate(0, 0, -1.0f / MOVE_COUNT);
                break;
        }
        Move_count--;
    }
    
    

    /// <summary>
    /// キー入力ごとに移動セット
    /// </summary>
    void InputMoveRequest(bool MP)
    {
        // 移動許可がない
        if (MP == false)
            return;

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        
        // 静止している状態
        if (Move_count == 0)
        {
            // 方向キーが押されると移動開始セット
            if (Math.Abs(x) > 0.00f || Math.Abs(y) > 0.00f)
            {
                // 壁判定システム
                int wall_like = map.WallInFront(cd.intPos);
                moveArrow_Set(x, y, wall_like);
            }
            else
                BEFORE_KEY = false;
        }
    }

    // 方向セット
    void moveArrow_Set(float x, float y, int wall_like)
    {
        // 前フレームで方向キーが押されているとreturn
        if (BEFORE_KEY == true)
            return;

        // メニュー禁止
        plycnt.isMenuAllow = false;

        // TODO:扉や壁紙のアクションに対応
        bool wall = true;
        if (wall_like == 0 || wall_like == 2) wall = false;  

        // 方向キーごとに移動する向きと、移動にかかる時間セット
        if (x == 1.0f)
        {
            Move_Arrow = Arrow.right;
            Move_count = MOVE_COUNT;
            BEFORE_KEY = true;
        }
        if (x == -1.0f)
        {
            Move_Arrow = Arrow.left;
            Move_count = MOVE_COUNT;
            BEFORE_KEY = true;
        }
        if (y == 1.0f)
        {
            if (wall == true)
            {
                Move_Arrow = Arrow.wall;
                Move_count = MOVE_COUNT - (MOVE_COUNT / 3);
                BEFORE_KEY = true;
                return;
            }
            Move_Arrow = Arrow.up;
            Move_count = MOVE_COUNT;
            BEFORE_KEY = true;
        }
        if (y == -1.0f)
        {
            Move_Arrow = Arrow.down;
            Move_count = MOVE_COUNT;
            BEFORE_KEY = true;
        }
    }

    // Use this for initialization
    void OnEnable()
    {
        // データクラス
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();

        // イベントシステム
        iv_sys = GameObject.Find("マップ管理").GetComponent<MapEventSystem>();

        // コントローラー
        plycnt = GameObject.Find("cdObject").GetComponent<PlayerController>();

        // 現在の外部からの参照用座標
        cd.intPos = new CommonData.Position(0, 0, 0, 1);

        // 復帰用のマップ座標設定読み込み
        if (cd.RetPosition.x != -1 && cd.RetPosition.y != -1)
        {
            cd.intPos = new CommonData.Position(cd.RetPosition.x, cd.RetPosition.y, cd.RetPosition.direction, cd.RetPosition.floor);
            gameObject.transform.position = new Vector3(cd.RetPosition.x, 0, -cd.RetPosition.y);
            gameObject.transform.rotation = Quaternion.Euler(0, cd.RetPosition.direction * 90f, 0);
        }

        initCount = 1;
    }
    
    /// <summary>
    /// 前回座標に現在座標をセット
    /// </summary>
    public void BeforePositionSet(CommonData.Position pos)
    {
        cd.BeforePosition = new CommonData.Position(pos.x, pos.y, pos.direction, pos.floor);
    }

    /// <summary>
    /// 前回フレーム座標(イベント、バトル用)に現在座標をセット
    /// </summary>
    /// <param name=""></param>
    public void BeforeFlamePositionSet(CommonData.Position pos)
    {
        cd.BeforeFlamePos = new CommonData.Position(pos.x, pos.y, pos.direction, pos.floor);
    }
    

}