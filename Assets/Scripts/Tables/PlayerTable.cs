﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerTable : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :    読み込みに関するメソッドと変数  :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// 読み込み終了
    /// </summary>
    public bool isEndRead;

    /// <summary>
    /// 主人公キャラのテーブル
    /// </summary>
    public List<StatusTable> st_Aresta = new List<StatusTable>();
    public List<StatusTable> st_Erina = new List<StatusTable>();
    public List<StatusTable> st_Petil = new List<StatusTable>();

    /// <summary>
    /// csv読み込み
    /// </summary>
    private void Awake()
    {
        

        FileStream table = new FileStream(Application.dataPath + "/StreamingAssets/Tables/PlayerData.csv", FileMode.Open, FileAccess.Read);
        try
        {
            //抜けると同時に削除
            using (StreamReader sr = new StreamReader(table))
            {
                //ファイルが空の場合
                if (table.Length == 0)
                {
                    print("ファイルが空でした。");
                    return;
                }

                //一行目はスルー
                sr.ReadLine();


                //一行ごとに読み込める
                while (!sr.EndOfStream)
                {
                    // ファイルから一行読み込む
                    var line = sr.ReadLine();

                    // データを分ける
                    var ar = line.Split(',');

                    // 出力
                    foreach (var value in ar)
                    {
                        print(value + ", ");
                    }

                    if (ar[0] == "アレスタ")
                        st_Aresta.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                            ar[4], ar[5], ar[6], ar[7],
                                            ar[8], ar[9], ar[10], ar[11], ar[12]));
                    if (ar[0] == "エリナ")
                        st_Erina.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                            ar[4], ar[5], ar[6], ar[7],
                                            ar[8], ar[9], ar[10], ar[11], ar[12]));
                    if (ar[0] == "ペテル")
                        st_Petil.Add(new StatusTable(ar[0], ar[1], ar[2], ar[3],
                                            ar[4], ar[5], ar[6], ar[7],
                                            ar[8], ar[9], ar[10], ar[11], ar[12]));


                }

            }
        }
        catch (System.IO.FileNotFoundException ex)
        {
            //FileNotFoundExceptionをキャッチした時
            print("ファイルが見つかりませんでした。");
            print(ex.Message);
            return;
        }
        catch (System.UnauthorizedAccessException ex)
        {
            //UnauthorizedAccessExceptionをキャッチした時
            print("必要なアクセス許可がありません。");
            print(ex.Message);
            return;
        }

        isEndRead = true;
    }
}

/*~----------------------------------/
:                                    :
:  レベルごとの増分そのものの構造体  :
:                                    :
/~----------------------------------*/
[System.Serializable]
public struct StatusTable
{
    /// <summary>
    /// 主人公キャラのレベル
    /// </summary>
    public int LV;

    /// <summary>
    /// 主人公キャラのHp増分
    /// </summary>
    public int HP;

    /// <summary>
    /// 主人公キャラのHp増分
    /// </summary>
    public int MP;

    /// <summary>
    /// 主人公キャラの魔力増分
    /// </summary>
    public int STR;

    /// <summary>
    /// 主人公キャラの素早さ増分
    /// </summary>
    public int AGI;

    /// <summary>
    /// 主人公キャラの賢さ増分
    /// </summary>
    public int INT;

    /// <summary>
    /// 主人公キャラの元気度増分
    /// </summary>
    public int VIT;

    /// <summary>
    /// 主人公キャラの運のよさ増分
    /// </summary>
    public int LUK;

    /// <summary>
    /// 主人公キャラのクリティカル度増分
    /// </summary>
    public int CRI;

    /// <summary>
    /// 主人公キャラの経験値増分
    /// </summary>
    public int EXP;

    /// <summary>
    /// 主人公キャラの累積経験値
    /// </summary>
    public int EXP_ALL;

    /// <summary>
    /// スキルの属性
    /// </summary>
    public enum Attribute
    {
        Normal, Fire, Plant, Water, Light, Dark
    }

    /// <summary>
    /// 状態異常(バトルのみ)
    /// </summary>
    public enum Abnormality
    {
        Normal, Poison, Freeze, Down
    }

    /// <summary>
    /// 主人公キャラの属性
    /// </summary>
    public Attribute ATR;
    
    /// <summary>
    /// 主人公キャラの名前
    /// </summary>
    public string Name;
    
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="name"></param>
    /// <param name="lv"></param>
    /// <param name="hp"></param>
    /// <param name="mp"></param>
    /// <param name="str"></param>
    /// <param name="agi"></param>
    /// <param name="_int"></param>
    /// <param name="vit"></param>
    /// <param name="luk"></param>
    /// <param name="cri"></param>
    /// <param name="exp"></param>
    /// <param name="atr"></param>
    public StatusTable(string name, string lv, string hp,
                       string mp, string str, string agi,
                       string _int, string vit, string luk,
                       string cri, string exp, string exp_all, string atr)
    {
        Name = name;
        LV = int.Parse(lv);
        HP = int.Parse(hp);
        MP = int.Parse(mp);
        STR = int.Parse(str);
        AGI = int.Parse(agi);
        INT = int.Parse(_int);
        VIT = int.Parse(vit);
        LUK = int.Parse(luk);
        CRI = int.Parse(cri);
        EXP = int.Parse(exp);
        EXP_ALL = int.Parse(exp_all);
        ATR = (Attribute)int.Parse(atr);
        
    }
}
